
#include "DSP_Config.h"
#include <stdio.h>
#include "fft.h"
#include "gmm.h"
#include "libmfcc.h"
#include <math.h>
#include <string.h>
//#define PI 3.141592
#define BUFFERSIZE 4096
int M=BUFFERSIZE;
int kk=0;
int startflag = 0;
int training = 1;
short X[BUFFERSIZE];
COMPLEX w[BUFFERSIZE];
COMPLEX B[BUFFERSIZE];
double Fs = 8000;
float feature;
float denominator = 0;
float numerator = 0;
float magnitude = 0;
float avg = 0;

float maxMag = 0.0;
int maxFreqIndex = 0;
float maxFreq = 0.0;
float spectrum[2048];
float maxFreqIndex_final = 0;

float fDict[47]; // 33 A1 to 81 A5
char notes[12][3];
float notegap = 0;
float accuracy = 0;
char baseNote[3] = "aaa";

int n;

int main()
{
 DSP_Init();
 int ii, mm, ll, yy;
 // Twiddle factor
 for(ii=0; ii<M; ii++){
	 w[ii].real = cos((float)ii/(float)M*PI);
	 w[ii].imag = sin((float)ii/(float)M*PI);
 }

 // calculate frequencies for each note
 for(yy=0; yy < 47; yy++) {
	 fDict[yy] = (float) (440.0*pow((1.059463094359),((33-69)+yy)));
 }

 // create notes array
 strcpy(notes[0], "A");
 strcpy(notes[1], "A#");
 strcpy(notes[2], "B");
 strcpy(notes[3], "C");
 strcpy(notes[4], "C#");
 strcpy(notes[5], "D");
 strcpy(notes[6], "D#");
 strcpy(notes[7], "E");
 strcpy(notes[8], "F");
 strcpy(notes[9], "F#");
 strcpy(notes[10], "G");
 strcpy(notes[11], "G#");


 while(1) {
	 if(startflag){
		 // Remove bias (DC offset)
		 avg = 0;
		 for(mm=0; mm<M; mm++){
			 avg = avg + X[mm];
		 }
		 // Measure the Magnitude of the input to find starting point
		 avg = avg/M;
		 magnitude = 0;
		 for(mm=0; mm<M; mm++) {
			 magnitude = magnitude + abs(X[mm]-avg);
		 }
		 if(magnitude>30000){
			for (ll=0; ll<M; ll++) {
				B[ll].real = X[ll];
				B[ll].imag = 0;
			}
			// FFT: B is input and output, w is twiddle factors
			fft(B, BUFFERSIZE, w);
			maxMag = 0.0;
			int k = 0;
			float sum[512];
			// down-sample to find harmonics and identify fundamental
			for(k = 0; k < BUFFERSIZE / 8; k++)  {
				sum[k] = B[k].real * B[2*k].real * B[3*k].real;
				spectrum[k] = B[k].real;
				spectrum[2*k] = B[2*k].real;
				spectrum[3*k] = B[3*k].real;

				// find fundamental frequency (maximum value in plot)
				if( sum[k] > maxMag && k > 0 )  {
					maxMag = sum[k];
					maxFreqIndex = k;
				}
			}
			maxFreqIndex_final = maxFreqIndex * 8000.0 / BUFFERSIZE;
			n = ceil(log(maxFreqIndex_final/440)/log(1.059463094359) + 36);
			notegap = fDict[n] - fDict[n-1];
			accuracy = (maxFreqIndex_final - fDict[n-1]) / notegap;
			if (accuracy < 0.5) {
				strcpy(baseNote, notes[(n - 1) % 12]);
				accuracy = accuracy + 0.5;
			} else {
				strcpy(baseNote, notes[(n) % 12]);
				accuracy = accuracy - 0.5;
			}
			accuracy = (int) (accuracy * 100 - 50);
		 }
		 startflag = 0;
	 }
 }
}

