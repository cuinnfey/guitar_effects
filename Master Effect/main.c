// Welch, Wright, & Morrow, 
// Real-time Digital Signal Processing, 2011
 
///////////////////////////////////////////////////////////////////////
// Filename: main.c
//
// Synopsis: Main program file for demonstration code
//
///////////////////////////////////////////////////////////////////////



#include "DSP_Config.h"
#include <math.h>

#define SAMPLING_RATE 24000
#define MAX_BUFFER_SIZE 24000

// OLD STUFF
int delayCheck = 1;

// KNOBS
// Delay knobs
int delayNob = 0;
int feedbackNob = 0;
int delay_enable = 0;
int masterVolume = 0; // unsued

// Chorus knobs
int chorus_vibrato = 0;
int chorus_vibrato_rate = 5;
int chorus_vibrato_depth = 5; // 5-10ms where 5 is fixed. Could go higher than 10 if we wanted
int chorus_enable = 0;

// Flanger knobs
int flanger_enable = 0;
int flanger_rate = 5;
int flanger_depth = 5;
int flanger_feedbackNob = 0;

// Tremolo knobs
int tremolo_depth = 0;
int tremolo_rate = 0;
int tremolo_enable = 0;

// shared knobs
int level = 0;

// INDEXING
// shared
int front = 0;
int current = 0;
int end = 0;
int outCurrent = 0;

// delay
int iterator = 0;

// INITIALIZATION
const int sizeOfDelayBuffer = MAX_BUFFER_SIZE;
int delayLeft[MAX_BUFFER_SIZE];
int delayRight[MAX_BUFFER_SIZE];

void main() {
// initialize DSP board
DSP_Init();
StartUp();
	while(1) {

	}
}

