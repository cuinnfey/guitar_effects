// Welch, Wright, & Morrow, 
// Real-time Digital Signal Processing, 2011

///////////////////////////////////////////////////////////////////////
// Filename: StartUp.c
//
// Synopsis: Placeholder for code run after DSP_Init()
//
///////////////////////////////////////////////////////////////////////

#include "DSP_Config.h"

#define SAMPLING_RATE 24000
#define MAX_BUFFER_SIZE 360

extern const int sizeOfDelayBuffer;
extern int delayLeft[MAX_BUFFER_SIZE];
extern int delayRight[MAX_BUFFER_SIZE];

void StartUp()
{
	int i;
	for (i = 0; i < sizeOfDelayBuffer; i++) {
		delayLeft[i] = 0;
		delayRight[i] = 0;
	}
}
