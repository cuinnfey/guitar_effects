// Welch, Wright, & Morrow, 
// Real-time Digital Signal Processing, 2011

///////////////////////////////////////////////////////////////////////
// Filename: ISRs.c
//
// Synopsis: Interrupt service routine for codec data transmit/receive
//
///////////////////////////////////////////////////////////////////////

#include "DSP_Config.h" 
#include <math.h>
  
// Data is received as 2 16-bit words (left/right) packed into one
// 32-bit word.  The union allows the data to be accessed as a single 
// entity when transferring to and from the serial port, but still be 
// able to manipulate the left and right channels independently.

#define LEFT  0
#define RIGHT 1
#define MAX_FEEDBACK 0.5
#define MIN_DELAY 560
#define MIN_VOLUME 0
#define MAX_VOLUME 10
#define SAMPLING_RATE 24000
#define MAX_BUFFER_SIZE 24000
#define M_PI acos(-1.0)

volatile union {
	Uint32 ABC;
	Int16 Channel[2];
} CodecDataIn, CodecDataOut;

// KEEPING TRACK
extern int delayCheck;

// CONVERTING TO DOUBLE
double sr = 0.0;
double chorus_vibrato_rate_double = 0.0;
double si = 0.0;
double chorus_vibrato_depth_double = 0.0;
double range = 0.0;

double flanger_rate_double = 0.0;
double flanger_depth_double = 0.0;

double tremolo_rate_double = 0.0;
double tremolo_depth_double = 0.0;

// NOBS
// delay nobs
extern int delayNob;
extern int feedbackNob;
extern int masterVolume; //unused
extern int delay_enable;

// chorus knobs
extern int chorus_vibrato;  // vibrato ==0 chorus == 1
extern int chorus_vibrato_rate;  // make sure has a range of 5-14Hz
extern int chorus_vibrato_depth; // 5-10ms where 5 is fixed. Could go higher than 10 if we wanted
extern int chorus_enable;

// flanger knobs
extern int flanger_rate;
extern int flanger_depth;
extern int flanger_enable;
extern int flanger_feedbackNob;

// tremolo knobs
extern int tremolo_rate;
extern int tremolo_depth;
extern int tremolo_enable;

//shared
extern int level;

// INDEXING
extern int front;
extern int current;
extern int end;
extern int outCurrent;

// delay
extern int iterator;

// CHORUS/VIBRATO
int sin_iterator = 0;
int current_effect_delay_index = 0;
int flanger_effect_delay_index = 0;

// INITIALIZATION
extern const int sizeOfDelayBuffer;
extern int delayLeft[MAX_BUFFER_SIZE];
extern int delayRight[MAX_BUFFER_SIZE];
int outLeft = 0;
int outRight = 0;

interrupt void Codec_ISR()
///////////////////////////////////////////////////////////////////////
// Purpose:   Codec interface interrupt service routine  
//
// Input:     None
//
// Returns:   Nothing
//
// Calls:     CheckForOverrun, ReadCodecData, WriteCodecData
//
// Notes:     None
///////////////////////////////////////////////////////////////////////
{                    
	CodecDataIn.ABC = ReadCodecData();
	if (chorus_enable) {

		// convert common variables to double
		sr = (double) SAMPLING_RATE;
		chorus_vibrato_rate_double = (double) chorus_vibrato_rate;
		si = (double) sin_iterator;
		chorus_vibrato_depth_double = (double) chorus_vibrato_depth;

		// calculate "range" of occilating-past sample selection from the buffer
		range = (((chorus_vibrato_depth_double - 5.0) / 2000.0) * sr);

		// get index of occliating-past sample from buffer to be played
		current_effect_delay_index = ((int) (sin(chorus_vibrato_rate_double / sr * 2.0 * M_PI * si) * range + (range + 0.005 * sr)));

		// *NOTE* "occilating-past" refers to the fact we are selecting a sample from the buffer that is not the BUFFERSIZE samples ago
		// but rather from BUFFERSIZE minus some occilating number that changes every iteration samples ago. This creates a subtle detuning effect
		// making the signal sound like there is a choir of slightly different voices singing the same part.

		// circular buffer cemantics
		if (current - current_effect_delay_index < 0) {
			outCurrent = (current - current_effect_delay_index) + MAX_BUFFER_SIZE;
		} else {
			outCurrent = current - current_effect_delay_index;
		}
		// select the previous samples
		outLeft = delayLeft[outCurrent];
		outRight = delayRight[outCurrent];

		// stereo output cemantics
		delayLeft[current] = CodecDataIn.Channel[0];
		delayRight[current] = CodecDataIn.Channel[1];

		// circular buffer cemantics
		current = (current + 1) % sizeOfDelayBuffer;
		sin_iterator++;

		// select chorus or vibrato. GUI knobs work best with int variables.
	 if (chorus_vibrato) {  // 0
		CodecDataOut.Channel[0] = (CodecDataIn.Channel[0] + ((int) (((double) level * outLeft) / 10.0)));
		CodecDataOut.Channel[1] = (CodecDataIn.Channel[1] + ((int) (((double) level * outRight) / 10.0)));
	 } else {  // 1
		CodecDataOut.Channel[0] = (((int) (((double) level * outLeft) / 10.0)));
		CodecDataOut.Channel[1] = (((int) (((double) level * outRight) / 10.0)));
	 }
	} else if (delay_enable) {

		// over-implemented. Front and end aren't really used, but helped to visualize the circular buffer
		// while writing the code
		if (delayNob != delayCheck) {
			front = current;
			end = (front + delayNob) % delayNob;
			delayCheck = delayNob;
			iterator = 0;
		}
		// buffer sample selection
		outLeft = delayLeft[current];
		outRight = delayRight[current];

		// feedback implementation
		delayLeft[current] = CodecDataIn.Channel[0] + ((int) ((double) (feedbackNob * outLeft)) / 10.0);
		delayRight[current] = CodecDataIn.Channel[1] + ((int) ((double) (feedbackNob * outRight)) / 10.0);

		// circular buffer cemenatics
		iterator++;
		current = (front + ((iterator)% delayNob)) % sizeOfDelayBuffer;

		// output cemantics
		CodecDataOut.Channel[0] = (CodecDataIn.Channel[0] + ((int) (((double) level * outLeft) / 10.0)));
		CodecDataOut.Channel[1] = (CodecDataIn.Channel[1] + ((int) (((double) level * outRight) / 10.0)));

	} else if (flanger_enable){

		// convert common variables to double
		sr = (double) SAMPLING_RATE;
		flanger_rate_double = (double) flanger_rate;
		flanger_rate_double = flanger_rate_double / 10.0;
		si = (double) sin_iterator;
		flanger_depth_double = 15.0;

		// calculate "range" of occilating-past sample selection from the buffer
		range = (((flanger_depth_double) / 2000.0) * sr);

		// get index of occliating-past sample from buffer to be played
		flanger_effect_delay_index = ((int) (sin(flanger_rate_double / sr * 2.0 * M_PI * si) * range + (range)));

		// circular buffer cemantics
		if (current - flanger_effect_delay_index < 0) {
			outCurrent = (current - flanger_effect_delay_index) + MAX_BUFFER_SIZE;
		} else {
			outCurrent = current - flanger_effect_delay_index;
		}

		// buffer sample selection
		outLeft = delayLeft[outCurrent];
		outRight = delayRight[outCurrent];

		// feedback implementation
		delayLeft[current] = CodecDataIn.Channel[0] + ((int) ((double) (flanger_feedbackNob * outLeft)) / 10.0);
		delayRight[current] = CodecDataIn.Channel[0] + ((int) ((double) (flanger_feedbackNob * outRight)) / 10.0);

		// circular buffer cemantics
		current = (current + 1) % sizeOfDelayBuffer;
		sin_iterator++;

		// output cemantics
		CodecDataOut.Channel[0] = (CodecDataIn.Channel[0] + ((int) (((double) level * outLeft) / 10.0)));
		CodecDataOut.Channel[1] = (CodecDataIn.Channel[1] + ((int) (((double) level * outRight) / 10.0)));

	} else if (tremolo_enable) {

		// convert common variables to double
		sr = (double) SAMPLING_RATE;
		tremolo_rate_double = (double) tremolo_rate;
		si = (double) sin_iterator;
		tremolo_depth_double = (double) tremolo_depth;

		// cicular buffer cemantics
		sin_iterator++;

		// modulate amplitude during output cemantics
		CodecDataOut.Channel[0] = (int) (CodecDataIn.Channel[0] * ((1 - tremolo_depth_double / 10.0) * sin(tremolo_rate_double / sr * 2.0 * M_PI * si) + (tremolo_depth_double / 10.0)));
		CodecDataOut.Channel[1] = (int) (CodecDataIn.Channel[1] * ((1 - tremolo_depth_double / 10.0) * sin(tremolo_rate_double / sr * 2.0 * M_PI * si) + (tremolo_depth_double / 10.0)));

	} else {  // clean output
		CodecDataOut.ABC = CodecDataIn.ABC;
	}

	/* end your code here */
	WriteCodecData(CodecDataOut.ABC);		// send output data to port
}

