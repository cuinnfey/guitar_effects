# Guitar effects


Most of the effect and GUI implementations are in the interrupt file ISRs.c

All effects were implemented in C within Code Composer Studio 6.1.3 on the TI OMAPL138 LCDK board. 
Each effect, except for the tuner, (Delay, Chorus, Vibrato, Flanger, Tremolo) 
is implemented in the interrupt file. Each effect works best at a sampling rate 
of 24000 Hz with a line input and output codec. The tuner uses a sampling frequency of 8000 Hz. 
All user changeable variables are routed to knobs in the GUI.

See "Guitar Effects Processing Final Report" for a more detailed description
